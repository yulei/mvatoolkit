import os
import shutil
from copy import deepcopy
import yaml
from tqdm import tqdm
from pathlib import Path


def opt_hyper_param(config, ):
    from sklearn.model_selection import ParameterGrid
    import numpy as np

    # rndm = config['rndm']

    hyper_params = {}
    for p in config['optimization']['hyper_parameters']:
        hyper_params.update(p)

    param_grid = {}
    for k, v in hyper_params.items():
        print(f"[Hyper-parameters Optimization] ==> {k}: [{v[0]}, {v[1]}]")

        param_grid.update({k: np.linspace(start=v[0], stop=v[1], num=v[2], endpoint=True, dtype=int)})

    param_grid = ParameterGrid(param_grid)
    print(f"[Hyper-parameters Optimization] ==> total iterations: {len(param_grid)}")

    out_script = f"""#!/bin/bash 
source {os.path.join(Path(__file__).parent.parent.absolute(), 'setup.sh')}
    """

    # generate grid
    cwd = os.getcwd()
    print(f"[Hyper-parameters Optimization] ==> Working directory: {cwd}")

    for i, p in tqdm(enumerate(param_grid)):
        # print(i,p)

        con = deepcopy(config)
        tag = f"job_{'_'.join([str(v) for v in p.values()])}"

        con['output_path'] = os.path.join(cwd, tag, 'Output')

        for par, val in p.items():
            con['hyper_parameters'][par] = str(val)

        if os.path.exists(con['output_path']):
            shutil.rmtree(con['output_path'])
        os.makedirs(con['output_path'])
        with open(os.path.join(cwd, tag, 'config.yaml'), mode='w') as file:
            yaml.dump(con, file)

        out_script += f"""
if [[ $1 == \"{i}\" ]]; then
    cd \"{os.path.join(cwd, tag)}\" || return
    # run.py process -c config.yaml && run.py eval -c config.yaml 
    {' && '.join([f"run.py single-process -c config.yaml -k {i}" for i in range(config['k_fold'])])}
    run.py eval -c config.yaml 
fi
        """

    # generate submit bash script
    with open(os.path.join(cwd, 'submit.sh'), mode='w') as file:
        file.write(out_script)

        import stat
        st = os.stat(os.path.join(cwd, 'submit.sh'))
        os.chmod(os.path.join(cwd, 'submit.sh'), st.st_mode | stat.S_IEXEC)

    # generate condor script
    out_script = f"""
Executable = {os.path.join(cwd, 'submit.sh')}
Arguments = $(Process)
Universe = vanilla
Notification = Complete
Log=log/log.$(Cluster)
Error=log/err.$(Process)
Output=log/out.$(Process)
rank = (OpSysName == "CentOS")
Queue {len(param_grid)}
    """
    with open(os.path.join(cwd, 'submit.condor'), mode='w') as file:
        file.write(out_script)

    if os.path.exists(os.path.join(cwd, 'log')):
        shutil.rmtree(os.path.join(cwd, 'log'))
    os.makedirs(os.path.join(cwd, 'log'))
