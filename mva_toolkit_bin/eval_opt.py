import os
from distutils.dir_util import copy_tree
import json
import pandas as pd
import numpy as np
import yaml
from pathlib import Path


def eval_hyper_param(config, in_dir: str, out_dir: str, ):
    print("==> Evaluation on hyper-parameter optimization")

    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    n_folds = config['k_fold']
    in_opt_dir = in_dir if in_dir else '.'

    results = []
    hyper_lists = [next(iter(p)) for p in config['optimization']['hyper_parameters']]
    for i, each_bdt_folder in enumerate(Path(in_opt_dir).glob('job_*')):
        df_tmp = pd.DataFrame(
            json.load(open(os.path.join(in_opt_dir, each_bdt_folder, config['output_path'], 'eval', 'result.json'))),
            index=[i]
        )

        each_con = {}
        with open(os.path.join(in_opt_dir, each_bdt_folder, 'config.yaml')) as f:
            each_con.update(yaml.safe_load(f))

        for p in config['optimization']['hyper_parameters']:
            p_name = next(iter(p))
            df_tmp[p_name] = float(each_con['hyper_parameters'][p_name])
            df_tmp['folder'] = os.path.join(in_opt_dir, each_bdt_folder)

        results.append(df_tmp)

    results = pd.concat(results)

    best_auc = results.iloc[results['auc'].idxmax()]
    best_sig = results.iloc[results['significance'].idxmax()]
    print(f"==> Load {len(results)} results.")
    print(f"[ Result ] ==> Best AUC: {best_auc['auc']}")
    print(best_auc)
    print(f"[ Result ] ==> Best Significance: {best_sig['significance']}")
    print(best_sig)

    # if only two hyperparameters, do plot!
    print(hyper_lists)
    if len(hyper_lists) == 2:
        import seaborn as sns
        import matplotlib.pyplot as plt
        def plot_hyper(df, z, name):
            mesh = pd.pivot_table(df, columns=hyper_lists[1], index=hyper_lists[0], values=[z])
            mesh.columns = mesh.columns.droplevel()
            mesh.sort_index(inplace=True, ascending=False)
            print(mesh)

            fig, ax = plt.subplots(1, 1, figsize=(8, 8), dpi=80)
            sns.heatmap(mesh, linewidths=1, cmap='coolwarm', ax=ax, cbar_kws={'format': '%.3f', 'label': name})
            plt.ylabel(hyper_lists[0])
            plt.xlabel(hyper_lists[1])
            plt.savefig(os.path.join(out_dir, f'{name}.svg'), format='svg')

        plot_hyper(results.loc[results['MaxDepth'] >= 2, :], 'auc', 'auc')
        plot_hyper(results.loc[results['MaxDepth'] >= 2, :], 'significance', 'significance')

    # do plot for best auc and best significance
    from mva_toolkit_bin.eval import eval_test
    for name, best in zip(['best_sig', 'best_auc'],[best_sig, best_auc]):
        best_con = {}
        with open(os.path.join(best['folder'], 'config.yaml')) as f:
            best_con.update(yaml.safe_load(f))

        best_path = os.path.join(best['folder'], best_con['output_path'], 'eval')
        if not os.path.isdir(best_path):
            print(f"==> {best_path} not exists")
            eval_test(config=best_con, in_dir=os.path.join(best['folder'], best_con['output_path']), do_plot=True)
        if not os.path.isdir(os.path.join(out_dir, name)):
            os.mkdir(os.path.join(out_dir, name))
        copy_tree(best_path, os.path.join(out_dir, name))
