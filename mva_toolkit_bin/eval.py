import os
import shutil
import json


def export_xml(config, in_dir: str, out_dir: str, name: str):
    print("==> Export")

    n_folds = config['k_fold']
    in_xml_dir = in_dir if in_dir else config['output_path']

    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    for n in range(n_folds):
        xml_dir = os.path.join(in_xml_dir, f'fold_{n}', 'weights', 'TMVAClassification_BDTG.weights.xml')
        out_xml_dir = os.path.join(out_dir, f'{name if name else "hh3l_BDTG"}_fold_{n}.xml')

        if os.path.isfile(xml_dir):
            shutil.copy(xml_dir, out_xml_dir)
            print(f'==> Export {xml_dir} to {out_xml_dir}.')
        else:
            print(f'==> {xml_dir} does not exist.')

    pass


def eval_test(config, in_dir: str, do_plot: bool = True):
    import ROOT
    import pandas as pd
    import numpy as np
    from sklearn import metrics
    from scipy import stats
    import matplotlib.pyplot as plt

    n_vars = len(config['training_parameters']['variables'])
    n_folds = config['k_fold']
    in_root_dir = in_dir if in_dir else config['output_path']

    out_d = os.path.join(in_root_dir, 'eval')
    if not os.path.isdir(out_d):
        os.mkdir(out_d)

    result = {
        'auc': 0.,
        'significance': 0.,
        'mva_score': -999,
    }

    def get_tree(tree_name: str, signal: bool = False):
        rdf_dict = {}
        for n in range(n_folds):
            r_dir = os.path.join(in_root_dir, f'fold_{n}', f'original_hist_{n}.root')
            xml_dir = os.path.join(in_root_dir, f'fold_{n}', 'weights', 'TMVAClassification_BDTG.weights.xml')

            if (not os.path.isfile(r_dir)) or (not os.path.isfile(xml_dir)):
                print(f'==> {r_dir} does not exist')
                continue
            else:
                print(f'==> Reading {r_dir}.')
                rdf = ROOT.RDataFrame(tree_name, r_dir)

                ROOT.gInterpreter.ProcessLine(f'''
                TMVA::Experimental::RReader model_{n} ("{xml_dir}");
                auto computeModel_{n} = TMVA::Experimental::Compute<{n_vars}, float>(model_{n});
                ''')
                var_names = ROOT.TMVA.Experimental.RReader(xml_dir).GetVariableNames()
                for var in var_names:
                    rdf = rdf.Redefine(var, f'(float)({var})')

                rdf = rdf.Define(
                    "BDTG",
                    eval(f'ROOT.computeModel_{n}'),
                    eval(f'ROOT.model_{n}.GetVariableNames()'),
                ).Redefine("BDTG", "BDTG.front()")

                df = pd.DataFrame(rdf.AsNumpy(["BDTG", config["sample_weight"]]))
                df['classID'] = 1 if signal else 0
                rdf_dict.update({f'fold_{n}': df})

                # rdf.Snapshot('nominal', os.path.join(out_d, f'out_{tree_name}_{n}.root'))

        rdf_dict.update({'total': pd.concat([df for df in rdf_dict.values()])})

        return rdf_dict

    rdf_train = {
        'sig': get_tree('sig_train', signal=True),
        'bkg': get_tree('bkg_train', signal=False),
    }

    rdf_test = {
        'sig': get_tree('sig_test', signal=True),
        'bkg': get_tree('bkg_test', signal=False),
    }

    """
    ROC Curve
    """

    def get_roc(dfs, dfb):
        df = pd.concat([dfs, dfb])
        fpr, tpr, _ = metrics.roc_curve(
            y_true=df['classID'],
            y_score=df['BDTG'],
            sample_weight=df['weight'],
            pos_label=1,
        )
        auc = np.trapz(tpr, fpr)
        # auc = 1.0
        return fpr, tpr, auc

    plots = {ks: get_roc(vs, vb) for (ks, vs), (kb, vb) in zip(rdf_test['sig'].items(), rdf_test['bkg'].items())}

    mycolors = ['tab:blue', 'tab:green', 'tab:orange', 'tab:red']
    mylinestyles = ['dashed', 'dashed', 'dashed', 'solid']
    mylinewidth = [1.5, 1.5, 1.5, 2.5]
    fig, ax = plt.subplots(1, 1, figsize=(8, 6), dpi=80)
    for i, (key, value) in enumerate(plots.items()):
        if do_plot:
            plt.plot(
                value[0], value[1],
                label=f"{key:^7}: AUC = {value[2]:.3f}",
                color=mycolors[i],
                linestyle=mylinestyles[i],
                linewidth=mylinewidth[i],
            )

        if key == 'total':
            result['auc'] = value[2]

    if do_plot:
        plt.ylabel('Signal Eff.')
        plt.xlabel('Background InEff.')
        plt.legend(loc=4)
        plt.show()
        fig.savefig(os.path.join(out_d, 'roc.svg'), format='svg')

    """
    Significance Curve
    """
    bkg = rdf_test['bkg']['total'].to_numpy()
    sig = rdf_test['sig']['total'].to_numpy()

    bins = np.linspace(-1, 1, num=200, endpoint=True)
    hist_sig, _ = np.histogram(sig[:, 0], bins=bins, weights=sig[:, 1])
    hist_bkg, _ = np.histogram(bkg[:, 0], bins=bins, weights=bkg[:, 1])
    s = np.cumsum(hist_sig[::-1])[::-1]
    b = np.cumsum(hist_bkg[::-1])[::-1]

    sig_err = np.sqrt(np.histogram(sig[:, 0], bins=bins, weights=sig[:, 1] ** 2)[0])
    bkg_err = np.sqrt(np.histogram(bkg[:, 0], bins=bins, weights=bkg[:, 1] ** 2)[0])
    s_err = np.sqrt(np.cumsum(sig_err[::-1] ** 2)[::-1])
    b_err = np.sqrt(np.cumsum(bkg_err[::-1] ** 2)[::-1])

    significance = (s / np.sqrt(s + b))
    significance[np.isnan(significance)] = 0

    def sig_unc(s, b, ds, db):
        t1 = ((np.sqrt(s + b) - s / (2 * np.sqrt(s + b))) / (s + b) * ds) ** 2
        t2 = (-(s * 1. / (2 * np.sqrt(s + b)) / (s + b)) * db) ** 2

        return np.sqrt(t1 + t2)

    significance_err = sig_unc(s, b, s_err, b_err)
    significance_err[np.isnan(significance_err)] = 0

    significance_with_min_bkg = max([(y, x) for x, y in enumerate(significance) if b[x] > 1.0])

    result['significance'] = significance_with_min_bkg[0]
    result['significance_no_protect'] = max(significance)
    result['mva_score'] = bins[1 + significance_with_min_bkg[1]]
    result['mva_score_no_protect'] = bins[1 + np.argmax(significance)]

    if do_plot:
        fig, ax = plt.subplots(1, 1, figsize=(8, 6), dpi=80)
        plt.plot(bins[1:], significance, color='#3776ab')
        plt.fill_between(
            bins[1:], significance - significance_err, significance + significance_err, alpha=0.35,
            edgecolor='#3776ab', facecolor='#3776ab', hatch='///', linewidth=0, interpolate=True
        )

        plt.vlines(
            x=result['mva_score'], ymin=0, ymax=result['significance'],
            colors='purple',
            label=f'max Sig. = {result["significance"]:.3f} at {result["mva_score"]:.2f}'
        )

        plt.ylabel('Significance')
        plt.xlabel('MVA score')
        plt.legend(loc=3)
        plt.show()
        fig.savefig(os.path.join(out_d, 'sig.svg'), format='svg')

    """
    Train Test Histogram
    """
    if do_plot:
        fig, (ax1, ax2) = plt.subplots(
            2, 1, figsize=(8, 10), dpi=80,
            sharex=True, gridspec_kw={'height_ratios': [4, 1]}
        )

        bins = np.linspace(-1, 1, num=25, endpoint=True)
        bin_center = (bins[:-1] + bins[1:]) / 2.
        bin_err = bins[1:] - bin_center
        hist_style = dict(
            bins=bins,
            density=True, alpha=0.75, histtype='step', linewidth=2.5
        )

        # for test
        bkg = rdf_test['bkg']['total'].to_numpy()
        sig = rdf_test['sig']['total'].to_numpy()

        hist_sig = np.histogram(sig[:, 0], bins=bins, weights=sig[:, 1], density=True)[0]
        hist_bkg = np.histogram(bkg[:, 0], bins=bins, weights=bkg[:, 1], density=True)[0]
        sf_s = np.sum(hist_sig) / np.sum(sig[:, 0])
        sf_b = np.sum(hist_bkg) / np.sum(bkg[:, 0])
        sig_err = np.sqrt(np.histogram(sig[:, 0], bins=bins, weights=sig[:, 1] ** 2)[0]) * sf_s
        bkg_err = np.sqrt(np.histogram(bkg[:, 0], bins=bins, weights=bkg[:, 1] ** 2)[0]) * sf_b
        ax1.errorbar(
            bin_center, hist_sig, yerr=sig_err, xerr=bin_err, color='red', label='sig: test',
            marker='.', alpha=0.75, elinewidth=2, markersize=10, ls='none')
        ax1.errorbar(
            bin_center, hist_bkg, yerr=bkg_err, xerr=bin_err, color='blue', label='bkg: test',
            marker='.', alpha=0.75, elinewidth=2, markersize=10, ls='none')

        # for train
        bkg = rdf_train['bkg']['total'].to_numpy()
        sig = rdf_train['sig']['total'].to_numpy()
        s, _, _ = ax1.hist(sig[:, 0], weights=sig[:, 1], color='red', label='sig: train', **hist_style)
        b, _, _ = ax1.hist(bkg[:, 0], weights=bkg[:, 1], color='blue', label='bkg: train', **hist_style)
        sf_s = np.sum(s) / np.sum(sig[:, 0])
        sf_b = np.sum(b) / np.sum(bkg[:, 0])
        s_err = np.sqrt(np.histogram(sig[:, 0], bins=bins, weights=sig[:, 1] ** 2)[0]) * sf_s
        b_err = np.sqrt(np.histogram(bkg[:, 0], bins=bins, weights=bkg[:, 1] ** 2)[0]) * sf_b
        ax1.fill_between(
            bin_center, s - s_err, s + s_err, alpha=0.5, facecolor='red', edgecolor='red',
            hatch='///', linewidth=0
        )
        ax1.fill_between(
            bin_center, b - b_err, b + b_err, alpha=0.5, facecolor='blue', edgecolor='blue',
            hatch='///', linewidth=0
        )

        ks_sig = stats.kstest(hist_sig, s)
        ks_bkg = stats.kstest(hist_bkg, b)

        print('signal: ', ks_sig)
        print('background: ', ks_bkg)

        def divide_unc(a, b, da, db):
            return np.sqrt((1. / b * da) ** 2 + (-(a / b ** 2) * db) ** 2)

        ratio_s = hist_sig / s
        ratio_b = hist_bkg / b
        ratio_s_unc = divide_unc(hist_sig, s, sig_err, s_err)
        ratio_b_unc = divide_unc(hist_bkg, b, bkg_err, b_err)
        ratio_s[np.isnan(ratio_s)] = 0.0
        ratio_b[np.isnan(ratio_b)] = 0.0
        ratio_s_unc[np.isnan(ratio_s_unc)] = 0
        ratio_b_unc[np.isnan(ratio_b_unc)] = 0

        # ratio plot
        ax2.errorbar(
            bin_center, ratio_s, yerr=ratio_s_unc, color='red', label='sig: test',
            marker='o', alpha=0.75, ls='none')
        ax2.errorbar(
            bin_center, ratio_b, yerr=ratio_b_unc, color='blue', label='bkg: test',
            marker='o', alpha=0.75, ls='none')
        ax2.set_ylim([0.75, 1.25])
        ax2.axhline(
            xmin=-1.0, xmax=1.0, y=1.0,
            c='lightgrey',
            ls='--'
        )
        ax2.set_ylabel('test / train')

        plt.xlabel('MVA score')
        ax1.legend(loc=1)
        plt.subplots_adjust(wspace=0, hspace=0.1)
        plt.show()
        fig.savefig(os.path.join(out_d, 'mva_hist.svg'), format='svg')

    """
    Output JSON
    """
    # Serializing json
    json_object = json.dumps(result, indent=4)

    # Writing to sample.json
    with open(os.path.join(out_d, 'result.json'), "w") as outfile:
        outfile.write(json_object)
