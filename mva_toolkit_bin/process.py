import os

import ROOT
from Config.EnumConfig import DataType
from TMVAToolkit.IO.InputReader import InputReaderProcessor
from TMVAToolkit.TrainProcessor import TrainProcessor


def process_data(config: dict, fold: int):
    load_data_args = dict(
        k_fold=config['k_fold'],
        k_fold_var=config['k_fold_var'],
        sample_weight=config['sample_weight'],
        variables=config['training_parameters']['variables'],
        train_selection=config['training_parameters']['region_selection'],
        eval_selection=config['evaluation_parameters']['region_selection'],
    )

    # read in signal
    sig_dataset = {}
    bkg_dataset = {}

    s = InputReaderProcessor(
        os.path.join(config['input_path'], config['training_parameters']['ntuple']),
        tree_name='nominal',
        name='Sig Reader',
        extra_selection=config['training_parameters']["signal_names"],
        eval_samples=config['evaluation_parameters']['extra_sig_samples'],
        **load_data_args
    )

    b = InputReaderProcessor(
        os.path.join(config['input_path'], config['training_parameters']['ntuple']),
        tree_name='nominal',
        name='Bkg Reader',
        extra_selection=config['training_parameters']["background_names"],
        eval_samples=config['evaluation_parameters']['extra_bkg_samples'],
        **load_data_args
    )

    sig_dataset.update({'train': s.get_data(DataType.train, fold=fold)})
    sig_dataset.update({'test': s.get_data(DataType.test, fold=fold)})
    bkg_dataset.update({'train': b.get_data(DataType.train, fold=fold)})
    bkg_dataset.update({'test': b.get_data(DataType.test, fold=fold)})

    if not os.path.isdir(os.path.join(config['output_path'], f'fold_{fold}')):
        os.makedirs(os.path.join(config['output_path'], f'fold_{fold}'), exist_ok=True)
    out_file_name = os.path.join(config['output_path'], f'fold_{fold}', f'original_hist_{fold}.root')
    snap_option = ROOT.RDF.RSnapshotOptions()
    snap_option.fMode = "UPDATE"
    snap_option.fOverwriteIfExists = True

    s.report.Print()
    b.report.Print()

    cols = ROOT.std.vector('std::string')()
    for b in [config['k_fold_var'], config['sample_weight'], *load_data_args['variables']]:
        cols.push_back(b)
    for name, data in zip(['sig', 'bkg'], [sig_dataset, bkg_dataset]):
        for key, value in data.items():
            value.Snapshot(f'{name}_{key}', out_file_name, cols, snap_option)
            # value.Sum('weight').GetValue()
            pass

    # ROOT.gROOT.CloseFiles()
    # ROOT.gROOT.Reset()
    return out_file_name


def process_fold(config: dict, fold: int):
    out_file_name = process_data(config=config, fold=fold)

    # out_file_name = r'Output/tmp_2.root'

    train = TrainProcessor(
        ntuple=out_file_name, out_dir=config['output_path'],
        sample_weight=config['sample_weight'],
        variables=config['training_parameters']['variables'],
        k_fold=config['k_fold'],
        fold=fold,
    )
    train.train(config['hyper_parameters'])

    print('--> Done')


if __name__ == '__main__':
    from Config.Control import load_config, cfg

    load_config(r'/Users/avencast/PycharmProjects/mvatoolkit/workspace/default_workspace/config.yaml')

    process_fold(config=cfg, fold=0)
