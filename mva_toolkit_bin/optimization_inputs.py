import os
import shutil
import time
import json
from copy import deepcopy
import yaml
import itertools
from pathlib import Path
import htcondor
import operator

# eval_value = 'auc'
eval_value = 'significance'


def mkdir(path, sec_path=None):
    if os.path.exists(path):
        shutil.rmtree(path)
    if sec_path is not None:
        os.makedirs(os.path.join(path, sec_path))
    else:
        os.makedirs(path)


def wait_for_complete(wait_time, cluster_id, schedd, itemdata):
    time.sleep(15)
    while True:
        ads = schedd.query(
            constraint=f"ClusterId == {cluster_id}",
            projection=["ClusterId", "ProcId", "Out", "JobStatus"],
        )
        if len(ads) == 0: return
        n_runs = len([ad["JobStatus"] for ad in ads if ad["JobStatus"] == 2])
        n_idle = len([ad["JobStatus"] for ad in ads if ad["JobStatus"] == 1])
        n_other = len([ad["JobStatus"] for ad in ads if ad["JobStatus"] > 2])
        print(f"-- {n_idle} idle, {n_runs}/{len(itemdata)} running... (wait for another {wait_time} seconds)")
        if n_other > 0:
            print(f"-- {n_other} jobs in other status, please check")
        if n_other > 0 and (n_runs + n_idle == 0):
            print(f"-- {n_other} jobs in other status, other's done, please check")
            return

        time.sleep(wait_time)


def run_loop(config, params, loop_num: int):
    # global vars
    run_py = os.path.join(Path(__file__).parent.parent.absolute(), 'run.py')

    # submit jobs
    print(f"==> [Loop {loop_num}] Do hyper scan on inputs: ")
    for p in params:
        print(f"\t -- {p}")

    job_dir = os.path.join(os.getcwd(), f'run_{loop_num}')
    mkdir(job_dir)
    os.makedirs(os.path.join(job_dir, 'log'))
    os.chdir(job_dir)

    jobs = {}
    schedd = htcondor.Schedd()
    for idx, removed_param in enumerate(params):
        print(f"==> [Loop {loop_num}] - [len {len(params) - 1}] remove input var: {removed_param} ")

        con = deepcopy(config)
        job_tag = f'job_{idx}'
        con['output_path'] = os.path.join(job_dir, job_tag, 'Output')
        con['training_parameters']['variables'] = [p for p in params if p != removed_param]
        mkdir(con['output_path'])
        with open(os.path.join(job_dir, job_tag, 'config.yaml'), mode='w') as file:
            yaml.dump(con, file)

        jobs.update({idx: {
            'vars': con['training_parameters']['variables'],
            'result_dir': os.path.join(con['output_path'], 'eval', 'result.json')
        }})

    # submit jobs using htcondor
    sub_job = htcondor.Submit({
        "executable": "/cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/bin/python",
        # "executable": "/bin/echo",
        "arguments": f"{run_py} single-process -c config.yaml -k $(fold_id)",
        "output": f"{os.path.join(job_dir, 'log')}/train_$(job_id)-$(fold_id).out",
        "error": f"{os.path.join(job_dir, 'log')}/train_$(job_id)-$(fold_id).err",
        "log": f"{os.path.join(job_dir, 'log')}/train_$(ClusterID).log",
        "rank": '(OpSysName == "CentOS")',
        "getenv": 'True',
    })

    itemdata = []
    for itr in itertools.product(range(con['k_fold']), range(len(params))):
        itemdata += [
            {'fold_id': str(itr[0]), 'job_id': str(itr[1]), 'initialdir': os.path.join(job_dir, f'job_{itr[1]}')}]

    submit_result = schedd.submit(sub_job, itemdata=iter(itemdata))

    print(f"==> Submitting {len(itemdata)} jobs to cluster {submit_result.cluster()}")

    # waiting for job complete
    wait_for_complete(30, submit_result.cluster(), schedd, itemdata)
    print(f"==> [Run {loop_num}] Training Done. Evaluate Now.")

    # eval jobs
    eval_job = htcondor.Submit({
        "executable": "/cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/bin/python",
        "arguments": f"{run_py} eval -c config.yaml",
        "output": f"{os.path.join(job_dir, 'log')}/eval_$(job_id).out",
        "error": f"{os.path.join(job_dir, 'log')}/eval_$(job_id).err",
        "log": f"{os.path.join(job_dir, 'log')}/eval_$(ClusterID).log",
        "rank": '(OpSysName == "CentOS")',
        "getenv": 'True',
    })

    itemdata = [{'job_id': str(p), 'initialdir': os.path.join(job_dir, f'job_{p}')} for p in range(len(params))]
    submit_result = schedd.submit(eval_job, itemdata=iter(itemdata))
    print(f"==> Submitting {len(itemdata)} eval jobs to cluster {submit_result.cluster()}")
    wait_for_complete(30, submit_result.cluster(), schedd, itemdata)

    job_result = {idx: json.load(open(value['result_dir']))[eval_value] for idx, value in jobs.items()}
    max_job = max(job_result.items(), key=operator.itemgetter(1))[0]
    best_params = jobs[max_job]['vars']
    print(f"==> [Run {loop_num}] Evaluation Done. Best Combination: ")

    job_result = [{params[idx]: json.load(open(value['result_dir']))[eval_value] for idx, value in jobs.items()}]

    return best_params, job_result


def plot_result(result):
    import numpy as np
    import pandas as pd
    import seaborn as sns
    import matplotlib.pyplot as plt
    import matplotlib.colors as colors

    sns.set_theme(style="white")

    df = pd.DataFrame(result)
    col_nan = df.isna().sum(axis=0)
    col_nan.sort_values(inplace=True, ascending=False)
    df = df[col_nan.index]
    mask = df.isna()

    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(20, 9))

    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(255, 135, as_cmap=True, sep=1)

    inclusive = json.load(open(os.path.join(os.getcwd(), 'run_inclusive', 'Output', 'eval', 'result.json')))
    max_value = inclusive[eval_value]
    plot_df = df / df.stack().max()
    text_df = (df - max_value) / max_value
    sns.heatmap(plot_df,
                mask=mask,
                cmap=cmap,
                norm=colors.PowerNorm(gamma=3),
                center=plot_df.stack().median(axis=0, skipna=True),
                annot=text_df,
                fmt=".2%",
                linewidths=.1,
                cbar=False,
                yticklabels=False
                )

    ly = len(df.index)
    lx = len(df.columns)
    line_style = dict(color='red', alpha=0.8)
    for x in range(lx):
        if x == 0:
            plt.axhline(lx - 2, (1 - 1 / float(lx)), 1 - 1 / float(lx) + 1. / float(lx), **line_style)
            continue
        if x == 1:
            plt.axvline(ly - x, x / float(ly), (x + 1) / float(ly), **line_style)
            continue

        plt.axhline(lx - x, (1 - x / float(lx)), 1 - x / float(lx) + 1. / float(lx), **line_style)
        plt.axvline(ly - x, x / float(ly), (x + 1) / float(ly), **line_style)

    plt.savefig(os.path.join(os.getcwd(), f'opt_input.svg'), format='svg')


def run_inclusive(config, ):
    from htcondor import dags
    diamond = dags.DAG()

    job_dir = os.path.join(os.getcwd(), 'run_inclusive')
    mkdir(job_dir)
    os.makedirs(os.path.join(job_dir, 'log'))
    os.chdir(job_dir)

    con = deepcopy(config)
    con['output_path'] = os.path.join(job_dir, 'Output')
    con['training_parameters']['variables'] = config['optimization']['training_variables']
    mkdir(con['output_path'])
    with open(os.path.join(job_dir, 'config.yaml'), mode='w') as file:
        yaml.dump(con, file)

    run_py = os.path.join(Path(__file__).parent.parent.absolute(), 'run.py')
    sub_job = diamond.layer(
        name="train",
        submit_description=htcondor.Submit({
            "executable": "/cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/bin/python",
            "arguments": f"{run_py} single-process -c config.yaml -k $(fold_id)",
            "output": f"{os.path.join(job_dir, 'log')}/train-$(fold_id).out",
            "error": f"{os.path.join(job_dir, 'log')}/train-$(fold_id).err",
            "log": f"{os.path.join(job_dir, 'log')}/train_$(ClusterID).log",
            "rank": '(OpSysName == "CentOS")',
            "getenv": 'True',
            "initialdir": os.path.join(job_dir),
        }),
        vars=[{"fold_id": k} for k in range(con['k_fold'])]
    )

    eval_job = sub_job.child_layer(
        name="eval",
        submit_description=htcondor.Submit({
            "executable": "/cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/bin/python",
            "arguments": f"{run_py} eval -c config.yaml",
            "output": f"{os.path.join(job_dir, 'log')}/eval.out",
            "error": f"{os.path.join(job_dir, 'log')}/eval.err",
            "log": f"{os.path.join(job_dir, 'log')}/eval_$(ClusterID).log",
            "rank": '(OpSysName == "CentOS")',
            "getenv": 'True',
            "initialdir": os.path.join(job_dir),
        })
    )

    dag_dir = Path('dag.submit')
    dag_file = dags.write_dag(diamond, dag_dir)
    dag_submit = htcondor.Submit.from_dag(str(dag_file), {'force': 1})
    os.chdir(dag_dir)
    schedd = htcondor.Schedd()
    with schedd.transaction() as txn:
        cluster_id = dag_submit.queue(txn)

    print(f"Inclusive DAGMan job cluster is {cluster_id}")


def opt_input_vars(config, ):
    if 'optimization' in config.keys():
        if 'training_variables' in config['optimization'].keys():
            params = config['optimization']['training_variables']
        else:
            print("==> Missing key: training_variables")
            exit(124)
    else:
        print("==> Missing key: optimization")
        exit(125)
    if len(params) <= 1:
        print("==> Only one var left, end of optimization")

    cwd = os.getcwd()
    run_inclusive(config)
    os.chdir(cwd)

    loop_num = 0
    opt_result = []
    while len(params) > 1:
        params, run_result = run_loop(config, params, loop_num)
        loop_num += 1
        opt_result += run_result

        os.chdir(cwd)

    plot_result(opt_result)

    # Serializing json
    json_object = json.dumps(opt_result, indent=4)

    # Writing to sample.json
    with open(os.path.join(cwd, 'result.json'), "w") as outfile:
        outfile.write(json_object)

    # pass
