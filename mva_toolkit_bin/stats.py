import ROOT
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.backends.backend_pdf import PdfPages


def load_data(config):
    rdf = ROOT.RDataFrame('nominal', os.path.join(config['input_path'], config['training_parameters']['ntuple']))

    extra_str_s = f"""
    std::vector<TString> name_s = {{ {','.join([f'"{n}"' for n in config['training_parameters']['signal_names']])} }};
    return (std::find(name_s.begin(), name_s.end(), TString(Sample_Name)) != name_s.end());
    """

    extra_str_b = f"""
    std::vector<TString> name_b = {{{','.join([f'"{n}"' for n in [*config['training_parameters']['background_names'], *config['evaluation_parameters']['extra_bkg_samples']]])}}};
    return (std::find(name_b.begin(), name_b.end(), TString(Sample_Name)) != name_b.end());
    """

    rdf_s = rdf.Filter(config['training_parameters']['region_selection']).Filter(extra_str_s, "Signal Sample")
    rdf_b = rdf.Filter(config['training_parameters']['region_selection']).Filter(extra_str_b, "Background Sample")

    col = [*config['training_parameters']['variables'], 'weight']

    df_s = pd.DataFrame(rdf_s.AsNumpy(columns=col))
    df_b = pd.DataFrame(rdf_b.AsNumpy(columns=col))

    return df_s, df_b


def separation_power_weighted(s, b, s_weights, b_weights):
    s_mean = weighted_mean(s, s_weights)
    b_mean = weighted_mean(b, b_weights)
    s_std = weighted_std(s, s_weights)
    b_std = weighted_std(b, b_weights)

    return (s_mean - b_mean) ** 2 / (s_std ** 2 + b_std ** 2)


def weighted_mean(data, weights):
    return np.sum(data * weights) / np.sum(weights)


def weighted_std(data, weights):
    mean = weighted_mean(data, weights)
    return np.sqrt(np.sum(weights * (data - mean) ** 2) / np.sum(weights))


def weighted_cov(x, y, weights):
    wx_mean = weighted_mean(x, weights)
    wy_mean = weighted_mean(y, weights)
    return np.sum(weights * (x - wx_mean) * (y - wy_mean)) / np.sum(weights)


def weighted_correlation_matrix(df, variables, weights):
    n = len(variables)
    corr_matrix = np.zeros((n, n))

    for i, var1 in enumerate(variables):
        for j, var2 in enumerate(variables):
            if i == j:
                corr_matrix[i][j] = 1
            elif i < j:
                cov = weighted_cov(df[var1], df[var2], weights)
                corr = cov / (weighted_std(df[var1], weights) * weighted_std(df[var2], weights))
                corr_matrix[i][j] = corr
                corr_matrix[j][i] = corr

    return corr_matrix


def plot_correlation_matrix(corr_matrix, save_path, variables):
    plt.figure(figsize=(len(variables) + 5, len(variables) + 5))
    sns.set(font_scale=1)
    sns.heatmap(
        corr_matrix, annot=True, xticklabels=variables, yticklabels=variables, cmap="coolwarm", vmin=-1, vmax=1,
        square=True, linewidths=.5, cbar_kws={"shrink": .5}
    )
    with PdfPages(save_path) as pdf:
        pdf.savefig(bbox_inches='tight')
    plt.close()


def do_stats(config):
    s, b = load_data(config)

    stats_path = os.path.join(config['output_path'], 'stats')
    os.makedirs(stats_path, exist_ok=True)

    # List of variables you want to calculate the separation power for
    variables = config['training_parameters']['variables']

    # Calculate the separation power for each variable
    df_sep = pd.DataFrame([], columns=['var', 'sep'])
    for var in variables:
        sep_power = separation_power_weighted(s[var], b[var], s['weight'], b['weight'])
        print(f"Separation power for {var}: {sep_power}")
        df_sep.loc[len(df_sep)] = [var, sep_power]
    df_sep.sort_values(by=['sep'], ascending=False, inplace=True)
    df_sep.to_csv(os.path.join(stats_path, 'separation.csv'))

    # plot the histogram of variable distribution for signal and background
    os.makedirs(os.path.join(stats_path, 'variable_distribution'), exist_ok=True)
    for var in variables:
        s_var_values = s[var]
        b_var_values = b[var]

        s_min, s_max = np.percentile(s_var_values, [0.0, 99.5])
        b_min, b_max = np.percentile(b_var_values, [0.0, 99.5])

        # combine the range of signal and background
        overall_min = min(s_min, b_min)
        overall_max = max(s_max, b_max)

        plt.figure(figsize=(8, 8))
        if var != "nJets_OR" and var != "FlavorCat":
            plt.hist(b[var], weights=b['weight'], bins=50, alpha=0.5, color='blue', label='Background', density=True,
                     range=(overall_min, overall_max))
            plt.hist(s[var], weights=s['weight'], bins=50, histtype='step', linestyle='dashed', color='red', label='Signal',
                     linewidth=2, density=True, range=(overall_min, overall_max))
        else:
            bin_width = 1.0  # change to your desired bin width
            bins = np.arange(overall_min, overall_max + bin_width, bin_width)

            plt.hist(b[var], weights=b['weight'], bins=bins, alpha=0.5, color='blue', label='Background', density=True)
            plt.hist(s[var], weights=s['weight'], bins=bins, histtype='step', linestyle='dashed', color='red',
                     label='Signal',
                     linewidth=2, density=True)
        plt.xlabel(var)
        plt.ylabel('A.U.')
        plt.legend(labels=["Background", "Signal"],
                   title=f'[Rank: {df_sep[df_sep["var"] == var].index.item() + 1}] Sep. = {df_sep[df_sep["var"] == var]["sep"].values[0]:.2%}')

        plt.savefig(os.path.join(stats_path, 'variable_distribution', f'{var}.pdf'))
        plt.close()

    # Calculate the covariance matrix
    weighted_corr_matrix_signal = weighted_correlation_matrix(s[variables], variables, s['weight'])
    weighted_corr_matrix_background = weighted_correlation_matrix(b[variables], variables, b['weight'])

    plot_correlation_matrix(weighted_corr_matrix_signal, os.path.join(stats_path, 'sig_corr.pdf'), variables)
    plot_correlation_matrix(weighted_corr_matrix_background, os.path.join(stats_path, 'bkg_corr.pdf'), variables)

    pass
