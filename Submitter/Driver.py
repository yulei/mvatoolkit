import os
import stat
from pathlib import Path


def write(s, s_path):
    script_file = open(s_path, 'w')
    script_file.write(s)
    script_file.close()

    os.chmod(s_path, os.stat(s_path).st_mode | stat.S_IEXEC)


class SubmitDriver:
    def __init__(self, config, work_dir: str, sub_name: str) -> None:
        self.base_dir = Path(__file__).parent.resolve().parent.absolute()
        self.config = config
        self.job_list = []
        self.sub_name = sub_name

        self.sub_dir = os.path.join(work_dir, self.sub_name)
        if not os.path.exists(self.sub_dir):
            os.makedirs(self.sub_dir)

    def _write_script_sub(self, run_command: str, job_name: str):
        template = f"""#!/bin/bash
source {os.path.join(self.base_dir, 'setup.sh')}
python3 {os.path.join(self.base_dir, 'run.py')} {run_command} 
        """

        script_path = os.path.join(self.sub_dir, f'{job_name}.sh')
        write(template, script_path)

    def _write_condor_sub(self, job_name: str):
        template = f"""Executable = {job_name}.sh
Universe = vanilla
getEnv = true
Notification = Complete
Log={os.path.join(f'log.{job_name}')}
Error={os.path.join(f'err.{job_name}')}
Output={os.path.join(f'out.{job_name}')}

rank = (OpSysName == "CentOS")
requirements = (machine == "bl-hd-1.phy.sjtulocal")

Queue
        """
        condor_path = os.path.join(self.sub_dir, f'{job_name}.sub')
        write(template, condor_path)

    def sub_jobs(self, job_name: str, n_fold: int):
        pass

    def loop_jobs(self):
        template = f''
        for job in self.job_list:
            self.sub_jobs(job_name=job['job_name'], n_fold=job['n_fold'])

            template += f"JOB {job['job_name']} {job['job_name']}.sub DIR {self.sub_name} \n"

        job_lists = ' '.join([j['job_name'] for j in self.job_list])
        return template, job_lists


class SubmitProcessor(SubmitDriver):
    def sub_jobs(self, job_name: str, n_fold: int):
        # write script shell
        self._write_script_sub(f"single-process -c {self.config['_config_abs_path']} -k {n_fold}", job_name)

        # write condor sub
        self._write_condor_sub(job_name)


class SubmitEval(SubmitDriver):
    def sub_jobs(self, job_name: str, n_fold: int = 0):
        # write script shell
        self._write_script_sub(f"eval -c {self.config['_config_abs_path']}", job_name)
        # write condor sub
        self._write_condor_sub(job_name)
        pass


class SubmitDagman:
    def __init__(self, dag_path: str, jobs: list[SubmitDriver]) -> None:
        self.jobs = jobs
        self.dag_path = dag_path
        self.dagname = 'mvatoolkit'

        if len(jobs) < 2:
            raise Exception("Please at least submit 2 jobs")

    def sub_dagman(self):
        template = ""
        job_order = ""
        for job in self.jobs:
            j, l = job.loop_jobs()
            template += j
            if job_order == "":
                job_order += f"PARENT {l} CHILD "
            else:
                job_order += f"{l}\nPARENT {l} CHILD "

        job_order = job_order[:job_order.rfind('\n')]

        write(template + job_order, os.path.join(os.getcwd(), self.dag_path, f'{self.dagname}.dag'))

        # os.system(f"condor_submit_dag -no_submit {self.dagname}")


if __name__ == '__main__':
    a = SubmitProcessor(None, 'test')
    a.job_list = [{'job_name': f'fold_{i}', 'n_fold': i} for i in range(3)]

    b = SubmitEval(None, 'eval_test')
    b.job_list = [{'job_name': 'eval_1', 'n_fold': None}, {'job_name': 'eval_2', 'n_fold': None}]

    d = SubmitEval(None, 'eval_test_d')
    d.job_list = [{'job_name': 'eval_d', 'n_fold': None}, {'job_name': 'eval_e', 'n_fold': None}]

    c = SubmitDagman([a, b])
    c.sub_dagman()
