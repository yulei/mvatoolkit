#!/usr/bin/env python3

import argparse

from Config.Control import load_config, cfg
from Config.EnumConfig import DriverType
from mva_toolkit_bin.eval import export_xml, eval_test
from mva_toolkit_bin.init import init
from mva_toolkit_bin.process import process_fold
from mva_toolkit_bin.optimization_hyperparameter import opt_hyper_param
from mva_toolkit_bin.eval_opt import eval_hyper_param
from mva_toolkit_bin.stats import do_stats


def k_fold_process(driver: DriverType):
    print('==> k_fold_process')
    if driver == DriverType.batch:
        import multiprocessing as mp
        print(f"==> Batch Mode: using {mp.cpu_count()} cpus")
        print(range(cfg['k_fold']))
        pool = mp.Pool()
        pool.starmap(process_fold, [(cfg, k) for k in range(cfg['k_fold'])])
        pool.close()

        print('==> Batch job done ')


if __name__ == '__main__':

    from pathlib import Path
    import sys

    base_dir = Path(__file__).parent.resolve().parent.absolute()
    sys.path.append(f'{base_dir}')

    par = argparse.ArgumentParser(prog='mvatoolkit', description='main entrance for mvatoolkit')
    subparsers = par.add_subparsers(title='modules', help='sub-command help', dest='command')

    # parser for init
    par_init = subparsers.add_parser('init', help='initiate a workspace for mvatoolkit')
    par_init.add_argument('-k', '--k_fold', metavar='n', type=int, required=True, help='define k in k-cv')
    par_init.add_argument('workspace_name', nargs='?', default='default_workspace', type=str, help="workspace name")

    # parser for k_fold-single_process
    par_stats = subparsers.add_parser('stats', help='analyzing stats for workspace')
    par_stats.add_argument('-c', '--config', type=str, help='path to config file')

    # parser for k_fold-single_process
    par_single = subparsers.add_parser('single-process', help='run a training for single fold')
    par_single.add_argument('-c', '--config', type=str, help='path to config file')
    par_single.add_argument('-k', '--k_fold', metavar='n', type=int, help='the k-th fold to run')

    # parser for k_fold_process
    par_process = subparsers.add_parser('process', help='run a k-fold training')
    par_process.add_argument('-c', '--config', type=str, help='path to config file')
    par_process.add_argument(
        '-d', '--driver', choices=['batch', 'condor'], default='batch', help='run program locally or on condor'
    )

    # parser for export
    par_exp = subparsers.add_parser('export', help='export training weights')
    par_exp.add_argument('-c', '--config', type=str, required=True, help='path to config file')
    par_exp.add_argument('-i', '--in_dir', type=str, help='path to trained xmls')
    par_exp.add_argument('-o', '--out_dir', type=str, required=True, help='path to export xmls')
    par_exp.add_argument('-n', '--name', type=str, help='name of export (prefix)')

    # parser for evaluation
    par_eval = subparsers.add_parser('eval', help='eval test tree')
    par_eval.add_argument('-c', '--config', type=str, required=True, help='path to config file')
    par_eval.add_argument('-i', '--in_dir', type=str, help='path to tmva_output.root')
    par_eval.add_argument(
        '--do_plot', action=argparse.BooleanOptionalAction, default=True,
        help='flag for doing evaluation plots'
    )

    # parser for hyperparameter optimization
    par_opt_param = subparsers.add_parser('opt_hyper_param', help='build hyper parameter optimization workspace')
    par_opt_param.add_argument('-c', '--config', type=str, required=True, help='path to config file')

    # parser for optimization evaluation
    par_opt_eval = subparsers.add_parser('eval_hyper_param', help='eval test tree')
    par_opt_eval.add_argument('-c', '--config', type=str, required=True, help='path to config file')
    par_opt_eval.add_argument('-i', '--in_dir', type=str, help='path to hyper parameters work dirs')
    par_opt_eval.add_argument('-o', '--out_dir', type=str, required=True, help='path to save evals')

    # parser for hyperparameter optimization
    par_opt_input = subparsers.add_parser('opt_input', help='run input variables optimization')
    par_opt_input.add_argument('-c', '--config', type=str, required=True, help='path to config file')

    args = par.parse_args()

    if args.command == 'init':
        init(**{k: v for k, v in vars(args).items() if k != 'command'})

    if args.command == 'stats':
        load_config(args.config)
        do_stats(config=cfg)

    if args.command == 'process':
        load_config(args.config)
        k_fold_process(DriverType[args.driver])

    if args.command == 'single-process':
        load_config(args.config)
        process_fold(config=cfg, fold=args.k_fold)

    if args.command == 'export':
        load_config(args.config)
        export_xml(config=cfg, out_dir=args.out_dir, in_dir=args.in_dir, name=args.name)

    if args.command == 'eval':
        load_config(args.config)
        eval_test(config=cfg, in_dir=args.in_dir, do_plot=args.do_plot)

    if args.command == 'opt_hyper_param':
        load_config(args.config)
        opt_hyper_param(config=cfg)

    if args.command == 'eval_hyper_param':
        load_config(args.config)
        eval_hyper_param(config=cfg, in_dir=args.in_dir, out_dir=args.out_dir)

    if args.command == 'opt_input':
        from mva_toolkit_bin.optimization_inputs import opt_input_vars
        load_config(args.config)
        opt_input_vars(config=cfg)

    else:
        pass
