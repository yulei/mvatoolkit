from enum import Enum


class DataType(Enum):
    none = 0
    train = 1
    test = 2
    apply = 3


class RunType(Enum):
    all = 0
    train = 1
    eval = 2
    apply = 3


class DriverType(Enum):
    batch = 0
    condor = 1


def type_helper(in_type):
    for data in in_type:
        print('{:15} = {}'.format(data.name, data.value))
