from collections import deque

import ROOT as r
import pandas as pd

from Config.EnumConfig import DataType

# r.EnableImplicitMT()
verbosity = r.Experimental.RLogScopedVerbosity(r.Detail.RDF.RDFLogChannel(), r.Experimental.ELogLevel.kInfo)


class InputReader:

    def __init__(self, input_path: str, tree_name: str, name: str = 'Reader'):
        self.name = name
        self.input_path = input_path if input_path.endswith('.root') else f'{input_path}.root'
        self.df = r.RDataFrame(tree_name, self.input_path)

    def __del__(self):
        # self.df.close()
        pass

    def get_path(self):
        return self.input_path


class InputReaderProcessor(InputReader):

    def __init__(
            self, input_path, tree_name: str,name: str,
            k_fold: int = None,
            k_fold_var: int = None,
            sample_weight: str = None,
            variables: list = None,
            train_selection: str = None,
            eval_selection: list = None,
            extra_selection: str = "1",
            eval_samples: list = None,
    ):
        super().__init__(input_path, tree_name, name)
        self.k_fold = k_fold
        self.k_fold_var = k_fold_var
        self.variables = variables
        self.sample_weight = sample_weight
        self.train_selection = train_selection
        self.eval_selection = eval_selection
        self.extra_selection = extra_selection
        self.eval_samples = eval_samples
        self.data = self._load_data()
        self.report = self.df.Report()

    def _load_data(self):

        extra_str = f"""
        std::vector<TString> names = {{ {','.join([f'"{n}"' for n in self.extra_selection])} }};
        return (std::find(names.begin(), names.end(), TString(Sample_Name)) != names.end());
        """

        eval_sample_list = self.extra_selection
        if (self.eval_samples is not None) and (len(self.eval_samples) > 0):
            eval_sample_list = [*self.extra_selection, *self.eval_samples]

        extra_eval_str = f"""
        std::vector<TString> names = {{ {','.join([f'"{n}"' for n in eval_sample_list])} }};
        return (std::find(names.begin(), names.end(), TString(Sample_Name)) != names.end());
        """

        data = {
            'train':
                self.df.Filter(self.train_selection, f'{self.name}:Train Selection').
                Filter(extra_str, f'{self.name}:Train Sample').
                Define('k_fold', f'EvtNum % {self.k_fold}').
                Define('wz_weight', f'(0.16121125697771188 + 0.86745172199254 / pow(( ((nJets_OR <= 4)*nJets_OR + (nJets_OR > 4)*4 ) +0.5), 0.2794680132699231)) * (mcChannelNumber == 364253) + (mcChannelNumber != 364253)').
                Redefine(self.sample_weight, f'{self.sample_weight} * wz_weight'),
            'eval':
                self.df.Filter(self.eval_selection, f'{self.name}:Eval Selection').
                Filter(extra_eval_str, f'{self.name}:Eval Sample').
                Define('k_fold', f'EvtNum % {self.k_fold}').
                Define('wz_weight',
                       f'(0.16121125697771188 + 0.86745172199254 / pow(( ((nJets_OR <= 4)*nJets_OR + (nJets_OR > 4)*4 ) +0.5), 0.2794680132699231)) * (mcChannelNumber == 364253) + (mcChannelNumber != 364253)').
                Redefine(self.sample_weight, f'{self.sample_weight} * wz_weight'),
        }

        return data

    def get_data(self, data_type: DataType, fold: int):
        idx = deque(range(self.k_fold))
        idx.rotate(fold)

        idx_dict = {
            'train': list(idx)[:-2],
            'test': list(idx)[-2],
            'apply': list(idx)[-1],
        }

        if data_type == DataType.train:
            return self.data['train'].Filter(
                f'k_fold != {idx_dict["test"]} &&  k_fold != {idx_dict["apply"]} ',
                f'{self.name}:Train k-fold')
        if data_type == DataType.test:
            return self.data['eval'].Filter(f'k_fold == {idx_dict["test"]}', f'{self.name}:Test k-fold')
        if data_type == DataType.apply:
            return self.data['eval'].Filter(f'k_fold == {idx_dict["apply"]}', f'{self.name}:Eval k-fold')

        return None


if __name__ == '__main__':
    import os
    from Config.Control import load_config, cfg

    load_config(r'../scripts/config.yaml')

    load_data_args = dict(
        k_fold=cfg['k_fold'],
        k_fold_var=cfg['k_fold_var'],
        sample_weight=cfg['sample_weight'],
        variables=cfg['training_parameters']['variables'],
        train_selection=cfg['training_parameters']['region_selection'],
        eval_selection=cfg['evaluation_parameters']['region_selection'],
    )

    b = InputReaderProcessor(os.path.join('Data', 'Sig3l_selected.root'), tree_name='total', **load_data_args)
    t = b.get_data(DataType.train, fold=0)
    tt = b.get_data(DataType.test, fold=0)

    print('--> Done')
