import os
import shutil

import ROOT
import pandas as pd

pd.options.mode.chained_assignment = None


class TrainProcessor:
    def __init__(
            self,
            ntuple: str,
            out_dir: str,
            k_fold: int = None,
            fold: int = None,
            sample_weight: str = None,
            variables: list = None,
    ):
        self.ntuple = ntuple
        self.fold_name = f'fold_{fold}'
        self.out_dir = os.path.join(out_dir, self.fold_name)
        if not os.path.exists(self.out_dir):
            os.makedirs(self.out_dir)

        self.k_fold = k_fold
        self.fold = fold
        self.variables = variables
        self.sample_weight = sample_weight

    def train(self, hyper_parameters):
        ROOT.TMVA.Tools.Instance()
        print((ROOT.TMVA.gConfig().GetIONames()).fWeightFileDir)

        # build data loader
        loader = ROOT.TMVA.DataLoader(self.fold_name)
        for cur_var in self.variables:
            loader.AddVariable(cur_var)

        f = ROOT.TFile(self.ntuple)

        loader.AddSignalTree(f.Get('sig_train'), 1.0, ROOT.TMVA.Types.ETreeType.kTraining)
        loader.AddSignalTree(f.Get('sig_test'), 1.0, ROOT.TMVA.Types.ETreeType.kTesting)
        loader.AddBackgroundTree(f.Get('bkg_train'), 1.0, ROOT.TMVA.Types.ETreeType.kTraining)
        loader.AddBackgroundTree(f.Get('bkg_test'), 1.0, ROOT.TMVA.Types.ETreeType.kTesting)
        loader.PrepareTrainingAndTestTree("", '!V:NormMode=EqualNumEvents')
        loader.SetWeightExpression(self.sample_weight)

        # build TMVA factory
        outfile = ROOT.TFile.Open(os.path.join(self.out_dir, 'tmva_output.root'), 'recreate')
        factory = ROOT.TMVA.Factory("TMVAClassification", outfile,
                                    ":".join([
                                        "!V",
                                        "!Silent",
                                        "Color",
                                        "DrawProgressBar",
                                        # "Transformations=I;D;P;G,D",
                                        "AnalysisType=Classification"]
                                    ))
        factory.BookMethod(
            loader, ROOT.TMVA.Types.kBDT, "BDTG",
            ":".join([f"{k}={v}" for k, v in hyper_parameters.items()])
        )

        factory.TrainAllMethods()
        factory.TestAllMethods()
        factory.EvaluateAllMethods()

        outfile.Close()
        print(f"===> wrote root file {self.out_dir}\n")
        print("===> TMVAClassification is done!\n")

        # put weight file into the correct directory
        if os.path.exists(os.path.join(self.out_dir, "weights")):
            shutil.rmtree(os.path.join(self.out_dir, "weights"))
        shutil.move(os.path.join(self.fold_name, "weights"), self.out_dir)
        shutil.rmtree(os.path.join(self.fold_name))

        f.Close()


if __name__ == '__main__':
    from Config.Control import load_config, cfg

    load_config(r'/Users/avencast/PycharmProjects/mvatoolkit/workspace/default_workspace/config.yaml')

    train = TrainProcessor(
        ntuple=f'/Users/avencast/PycharmProjects/mvatoolkit/workspace/default_workspace/Output/tmp_2.root',
        out_dir='./',
        sample_weight=cfg['sample_weight'],
        variables=cfg['training_parameters']['variables'])
    train.train(cfg['hyper_parameters'])

    print('--> Done')
